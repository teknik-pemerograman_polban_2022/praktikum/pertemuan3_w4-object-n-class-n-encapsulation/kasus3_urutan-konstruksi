public class KelasSatu {
    {
        System.out.println(11);
    }
    /*
     * Sistemnya mirip seperti static, hanya saja akan selalu dieksekusi ketika
     * objek baru dibuat
     */
    static {
        System.out.println(2);
    }
    /*
     * Static akan dieksekusi paling awal ketika class tersebut dibuat (dipanggil)
     * Namun static hanya akan dieksekusi 1x saja
     */

    public KelasSatu(int i) {
        System.out.println(3);
    }// Dipanggil oleh objek dua

    public KelasSatu() {
        System.out.println(4);
    }// Dipanggil oleh objek satu
}
